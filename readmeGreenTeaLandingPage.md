# Project Green Tea Landing Page
- <strong>Project Green Tea Landing Page</strong> là page giới thiệu về các loại trà và các sản phầm về trà
- Website này gồm 2 phần : 
1.  Page chính dành cho khách hàng, nơi có thể tìm hiểu về các sản phẩm trà và để lại contact
2. Page dành cho admin có các chức năng phục vụ cho việc đọc contact khách hàng

![CoverImg](imgMarkdown/title.PNG)

## 🌟 Các chức năng của Green Tea Landing Page trang chính

### 1. Để lại contact

#### 🧱 Trang chính có chức năng giới thiệu về các sản phẩm về trà và có phần send contact nếu muốn được tư vấn hay mua bán sản phầm

- Giao diện của Form Contact như sau

![contactForm](imgMarkdown/contactForm.PNG)

- Sau khi điền đầy đủ các thông tin như Name, Email, Message. Thì bấm nút <button style="background-color: #68763F; color: white" >SEND</button> để gửi thông tin cá nhân cho phía bán hàng

## ✨ Các chức năng của Green Tea Landing Page Trang ADMIN

### 1. Danh sách Liên hệ

#### 📰 Sau khi khách hàng điền thông tin và gửi contact cho phía bán hàng, Bên bán hàng sẽ nhận được những thông tin đó nhờ vào trang Danh sách Liên Hệ

![listContact](imgMarkdown/listContact.PNG)

- Danh sách liên hệ này hiển thị tất cả các thông tin của khách hàng quan tâm đến sản phẩm. Có 2 chức năng chính trong danh sách liên hệ đó là <button style="background-color: green; color: white" >Sửa</button> và <button style="background-color: red; color: white" >Delete</button>. 2 chức năng này sẽ được mô tả chi tiết ở phần sau

### 2. Sửa liên hệ

#### ✏️ Sau khi bấm vào nút <button style="background-color: green; color: white" >Sửa</button> sẽ được đưa đến trang sửa contact có giao diện như sau

![editContact](imgMarkdown/editContact.PNG)

- Ở trang này sẽ hiện các thông tin trước đó được gửi đến. Nếu muốn sửa bất kì thông tin nào, chỉ cần edit lại và bấm nút <button style="background-color: green; color: white" >Save Data</button>

### 3. Xóa liên hệ

#### ⛔ Sau khi bấm vào nút <button style="background-color: red; color: white" >Delete</button> sẽ thực hiện chức năng xóa contact

![deleteContact](imgMarkdown/deleteContact.PNG)

- Một cửa sổ hiện lên thông báo "bạn có chắc chắn muốn xóa liên hệ này!", để tránh việc đó chỉ là một tai nạn, hoặc thông tin đó vẫn còn cần thiết. Bấm OK để xóa và Cancel để hủy xóa

## 💻 Công nghệ đã dùng
+ Front-End:
    - 1. [Boostrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
    - 2. Javascript
+ Back-end:
+ KIT:
+ DB: